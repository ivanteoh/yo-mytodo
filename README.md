# yo-mytodo

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1. This is used in the tutorial at [Yeoman tutorial](http://yeoman.io/codelab/index.html)

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
